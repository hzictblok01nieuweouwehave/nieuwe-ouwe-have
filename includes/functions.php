<?php

	function isValidID($mysqli, $id) {
		if(is_numeric($id)) {
			$sql = 'SELECT * FROM oh_transactions WHERE ID = "' . $id . '"';
			$result = $mysqli->query($sql);

			return ($result->num_rows > 0);
		}

		return false;
	}

	function toTimestamp($value) {
		$date = DateTime::createFromFormat('Ymd', $value);

        return $date->getTimestamp();
	}

    function toDate($timestamp) {
        return date("d-m-Y", $timestamp);
    }

	function formatTransaction($transaction) {
    	// Boekcode omzetten
		$boekcodes = array(
			"ac" => "Acceptgiro",
			"ba" => "Betaalautomaat",
			"bc" => "Betalen contactloos",
			"bg" => "Bankgiro opdracht",
			"cb" => "Crediteuren betaling",
			"db" => "Diverse boekingen",
			"eb" => "Bedrijven Euro-incasso",
			"ei" => "Euro-incasso",
			"fb" => "FiNBOX",
			"ga" => "Geldautomaat Euro",
			"gb" => "Geldautomaat VV",
			"id" => "iDeal",
			"kh" => "Kashandeling",
			"ma" => "Machtiging",
			"sb" => "Salaris betaling",
			"sp" => "Spoedbetaling",
			"tb" => "Eigen rekening"
		);

		$transaction["Boekcode"] = $boekcodes[$transaction["Boekcode"]];

		return $transaction;
	}

    function generateName($voornaam, $tussenvoegel, $achternaam) {
        if(!empty($tussenvoegel)) {
            return $voornaam . ' ' . $tussenvoegel . ' ' . $achternaam;
        } else {
            return $voornaam . ' ' . $achternaam;
        }
    }

    function translateReservationStatus($status) {
        if($status == 0) {
            return "In afwachting";
        } else if ($status == 1) {
            return "Geweigerd";
        } else if($status == 2) {
            return "Geaccepteerd";
        }
    }

	function cleanInput($input) {
		return preg_replace("/[^[:alnum:][:space:].]/ui", '', $input);
	}

	function validateInput($input, $min, $max) {
		if(isset($input) && !empty($input)) {
			if((strlen($input) >= $min) && (strlen($input) <= $max)) {
				return true;
			}
		}
		return false;
	}

	function validateNumber($input, $min, $max) {
		if(isset($input) && !empty($input)) {
			if ((strlen($input) >= $min) && (strlen($input) <= $max) && (($num = filter_var($input, FILTER_VALIDATE_FLOAT)) !== false)) {
			    return true;
			}
		}
		return false;
	}

    function validateDate($date, $format) {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
	
	// Acces role function	
	function hasacces($pageid) {
		
		if(!isset($_SESSION['id'])) {
			
			return false;
			
		} else {
		
		global $dataManager;
		
			$dataManager->rawQuery("SELECT * FROM oh_permissions WHERE userid = '".$_SESSION['id']."' AND taskid = '".$pageid."'");
		
		if ($dataManager->count > 0) {
			
			return true;
		
		} else {
		
		return false;
		
		}
			
		}
	}
// constant time string compare
	function isEqual($str1, $str2) {
		
    	$n1 = strlen($str1);
    	
		if (strlen($str2) != $n1) {
        	
			return false;
    	}
    	
		for ($i = 0, $diff = 0; $i != $n1; ++$i) {
        	
			$diff |= ord($str1[$i]) ^ ord($str2[$i]);
    	}
    		
			return !$diff;
	} 