<?php

#################################################################
### Automatic invoices for all users with a boat ################
####                                                        #####
####               Made By: Joost van Crugten   			#####
####                                                        #####
#################################################################

require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';

$pageid = 14;

if (hasacces($pageid) == true) {
?>
<!DOCTYPE html>
<html lang="nl">
<head>

<?php include_once 'includes/head.php'; ?>

<title><?php echo SITE_TITLE; ?>- Automatisch facturen naar leden sturen</title>

</head>

<body>

<?php include_once 'includes/wrapper.php'; ?>

<!-- Sidebar -->
<?php include_once 'includes/sidebar.php'; ?>
<!-- /#sidebar-wrapper --> 

<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-header">
        <h1>Automatisch facturen versturen</h1>
      </div>
      <p>Hieronder kunt u automatisch een factuur versturen naar alle leden die een boot bezitten en die in de haven hebben liggen.
	  
                          <ul class="nav nav-tabs">
                        <li role="presentation"><a href="invoices.php">Facturen</a></li>
                        <li role="presentation"><a href="invoices-add.php">Enkele factuur toevoegen</a></li>
                        <li role="presentation" class="active"><a href="invoiceall-add.php">Massa factuur versturen</a></li>
                        <li role="presentation"><a href="priceCategories-add.php">Prijs Categorieen toevoegen (enkele facturen)</a>
                        <li role="presentation"><a href="priceCategoriesall-add.php">Prijs Categorieen toevoegen (massa facturen)</a>                        <li role="presentation"><a href="priceCategories-remove.php">Prijs Categorieen verwijderen</a>
                    </ul>
<?php

if (isset($_POST['submit'])) {

	// # BEGIN: Showing longest ship and other ships ####

	$userquery = $dataManager->rawQuery("SELECT id FROM oh_members");
	foreach($userquery as $userid) { // Shows the id of all users and puts them into variable 'userid'

		// Uses variable userid to select all ships that are binded to

		$longestship = $dataManager->rawQuery("SELECT *, MAX(Lengte) as Lengte FROM oh_member_ship, oh_ships WHERE oh_member_ship.Member_ID = '" . $userid['id'] . "' AND oh_member_ship.Ship_ID = oh_ships.id");
		if ($dataManager->count > 0) { // Excecutes only if someone has a ship
			foreach($longestship as $ownerlongest) {

				// # BEGIN: Adding Invoice ####
				// Creates a new invoice in the database (per user)

				$dataManager->rawQueryOne("INSERT INTO oh_invoices (Lid_ID, Datum, lastautodate) VALUES ('" . $ownerlongest['Member_ID'] . "', '" . date("Y-m-d") . "', '" . date("Y-m-d") . "')");
				$dataManager->rawQueryOne("UPDATE oh_invoices SET lastautodate = '" . date("Y-m-d") . "'");
				$invoiceid = $dataManager->rawQueryOne("SELECT ID FROM oh_invoices WHERE Datum = '" . date("Y-m-d") . "' AND Lid_ID = '" . $ownerlongest['Member_ID'] . "'");

				// # END: Adding Invoice ####

				$ownerlongest = $ownerlongest['Lengte'];

				// echo "Het langste schip:" . $ownerlongest;

				$contributie = $dataManager->rawQueryOne("SELECT * FROM oh_price_category WHERE naam = 'Contributie " . date("Y") . "'");
				$firstboatdata = $dataManager->rawQueryOne("SELECT * FROM oh_price_category WHERE naam = 'Ligplaats eerste boot " . date("Y") . "'");
				$allboatdata = $dataManager->rawQueryOne("SELECT * FROM oh_price_category WHERE naam = 'Ligplaats overige boten " . date("Y") . "'");

				// Adds first boat to the invoice

				$dataManager->rawQueryOne("INSERT INTO oh_invoices_line (Factuur_ID, Categorie_ID, Aantal, Bedrag) VALUES ('" . $invoiceid['ID'] . "', '".$firstboatdata['ID']."', '".$ownerlongest."', '" . $firstboatdata['PrijsPerEenheid'] . "')");
				$shipsbutlongest = $dataManager->rawQuery("SELECT * FROM oh_member_ship, oh_ships WHERE oh_member_ship.Member_ID = '" . $userid['id'] . "' AND oh_member_ship.Ship_ID = oh_ships.id AND oh_ships.Lengte NOT LIKE '" . $ownerlongest . "'");
				$cnt = $dataManager->rawQueryOne("SELECT Lengte FROM oh_member_ship, oh_ships WHERE oh_member_ship.Member_ID = '" . $userid['id'] . "' AND oh_member_ship.Ship_ID = oh_ships.id AND oh_ships.Lengte NOT LIKE '" . $ownerlongest . "'");
				
				foreach($shipsbutlongest as $ownerships) {

					// $ownerships = $ownerships['Lengte'] . "<br />";
					// echo "Alle niet langste schepen:" . $ownerships;

				if ($cnt['Lengte'] > 0) {

					// Adds all boats but the longest to the invoice

					$dataManager->rawQueryOne("INSERT INTO oh_invoices_line (Factuur_ID, Categorie_ID, Aantal, Bedrag) VALUES ('" . $invoiceid['ID'] . "', '" . $allboatdata['ID'] . "', '" . $ownerships['Lengte'] . "', '" . $allboatdata['PrijsPerEenheid'] . "')");
				}

				}

				$dataManager->rawQueryOne("INSERT INTO oh_invoices_line (Factuur_ID, Categorie_ID, Aantal, Bedrag) VALUES ('" . $invoiceid['ID'] . "', '" . $contributie['ID'] . "', '1', '" . $contributie['PrijsPerEenheid'] . "')");
			}
		}
	}

	// # END: Showing longest ship and other ships ####

}

$checkdate = $dataManager->rawQueryOne("SELECT lastautodate FROM oh_invoices");
$oneyearcheck = date("Y-m-d", time() + 31622000); // 366 Day = 366*24*60*60 = 31622000

?>
<?php

if ($oneyearcheck <= date("Y-m-d") or $checkdate == '') {
}
else {
?>
      <div class="alert alert-info">U kunt deze facturen maar één keer per jaar versturen, vandaar dat het nu niet mogelijk is.</div>

<?php
} ?>
      <form class="clearfix horizontalSearchForm" id="addInvoice" role="form"  method="POST" enctype="multipart/form-data">
        <div class="form-group col-md-6">
          <label>Laatste keer facturen verstuurd:</label>
          <div><?php echo $checkdate['lastautodate']; ?></div>
        </div>
        <div class="form-group col-md-2 pull-right">
<?php
if ($oneyearcheck <= date("Y-m-d") or $checkdate == '') {
?>
          <button type="submit" class="btn btn-primary" name="submit">Facturen versturen</button>
		  
<?php } else { ?>

          <button type="button" class="btn btn-error disabled">Facturen versturen</button>
		  
 <?php } ?>
 
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>

<!-- /#page-content-wrapper --> 

<!-- /#wrapper --> 

<!-- Footer -->
<?php
include_once 'includes/footer.php';

?>
<?php 

} else {
	
	header("Location: index.php");	
}
?>
</body>
</html>