-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 01 okt 2015 om 15:32
-- Serverversie: 5.6.26
-- PHP-versie: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ouwehaven`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_balances`
--

CREATE TABLE IF NOT EXISTS `oh_balances` (
  `ID` int(11) unsigned NOT NULL,
  `Saldo` float NOT NULL,
  `Datum` date NOT NULL,
  `Startmeting` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_balances`
--

INSERT INTO `oh_balances` (`ID`, `Saldo`, `Datum`, `Startmeting`) VALUES
(3, 500, '2015-06-23', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_cashbook`
--

CREATE TABLE IF NOT EXISTS `oh_cashbook` (
  `ID` int(11) NOT NULL,
  `Beschrijving` varchar(50) NOT NULL,
  `Datum` date NOT NULL,
  `Bedrag` float NOT NULL,
  `Afzender` varchar(50) NOT NULL,
  `Ontvanger` varchar(50) NOT NULL,
  `Code` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_cashbook`
--

INSERT INTO `oh_cashbook` (`ID`, `Beschrijving`, `Datum`, `Bedrag`, `Afzender`, `Ontvanger`, `Code`) VALUES
(13, 'Pennen', '2015-08-12', 5, 'Henk', 'Jan', 'C'),
(17, 'Tshirts', '2015-05-17', 13, 'Thijs', 'Raymon', 'D'),
(19, 'Laptop', '2015-05-23', 2600, 'Jan', 'Pietje', 'D'),
(20, 'jjjlkkjlkjl', '2015-09-30', 120, 'joost', 'joop', 'D');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_categories`
--

CREATE TABLE IF NOT EXISTS `oh_categories` (
  `ID` int(11) unsigned NOT NULL,
  `Naam` varchar(128) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_categories`
--

INSERT INTO `oh_categories` (`ID`, `Naam`) VALUES
(2, 'Spullen'),
(3, 'Overig');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_contributie`
--

CREATE TABLE IF NOT EXISTS `oh_contributie` (
  `id` int(11) NOT NULL,
  `jaar` int(11) NOT NULL,
  `contributie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_invoices`
--

CREATE TABLE IF NOT EXISTS `oh_invoices` (
  `ID` int(11) NOT NULL,
  `Lid_ID` int(11) NOT NULL DEFAULT '0',
  `Datum` date NOT NULL,
  `Betaald` int(3) NOT NULL DEFAULT '0',
  `DatumBetaald` date DEFAULT NULL,
  `BetaalWijze` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_invoices`
--

INSERT INTO `oh_invoices` (`ID`, `Lid_ID`, `Datum`, `Betaald`, `DatumBetaald`, `BetaalWijze`) VALUES
(1, 7, '2015-06-02', 0, NULL, '0'),
(2, 30, '2015-06-04', 0, NULL, '0'),
(3, 8, '2015-05-29', 1, NULL, '0'),
(4, 7, '2015-06-04', 0, NULL, '0'),
(5, 7, '2015-09-28', 0, NULL, '0'),
(6, 7, '2015-09-30', 0, NULL, '0'),
(7, 7, '2015-09-30', 0, NULL, '0'),
(8, 1, '2015-09-30', 0, NULL, '0');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_invoices_line`
--

CREATE TABLE IF NOT EXISTS `oh_invoices_line` (
  `ID` int(11) NOT NULL,
  `Factuur_ID` int(11) DEFAULT NULL,
  `Categorie_ID` int(11) DEFAULT NULL,
  `Aantal` int(11) DEFAULT NULL,
  `Bedrag` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_invoices_line`
--

INSERT INTO `oh_invoices_line` (`ID`, `Factuur_ID`, `Categorie_ID`, `Aantal`, `Bedrag`) VALUES
(1, 1, 1, 1, 15),
(2, 2, 3, 2, 50),
(3, 2, 1, 1, 15),
(5, 3, 2, 2, 40.5),
(6, 4, 2, 2, 50),
(7, 4, 1, 1, 72),
(8, 5, 1, 2147483647, 212121000000),
(9, 6, 1, 1212121, 12),
(10, 8, 1, 12, 121);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_members`
--

CREATE TABLE IF NOT EXISTS `oh_members` (
  `ID` int(11) NOT NULL,
  `User_ID` bigint(20) unsigned DEFAULT NULL,
  `user_login` varchar(60) NOT NULL,
  `user_pass` varchar(64) NOT NULL,
  `level` int(11) NOT NULL,
  `user_registered` datetime NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_activation_key` varchar(60) NOT NULL,
  `user_status` int(11) NOT NULL,
  `Voornaam` varchar(64) NOT NULL DEFAULT '',
  `Achternaam` varchar(64) NOT NULL DEFAULT '',
  `Tussenvoegsel` varchar(16) DEFAULT '',
  `Adres` varchar(128) NOT NULL DEFAULT '',
  `Postcode` varchar(7) NOT NULL DEFAULT '',
  `Woonplaats` varchar(128) NOT NULL DEFAULT '',
  `Telefoonnummer` varchar(16) DEFAULT '',
  `IBAN` varchar(34) DEFAULT '',
  `Actief` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_members`
--

INSERT INTO `oh_members` (`ID`, `User_ID`, `user_login`, `user_pass`, `level`, `user_registered`, `user_email`, `user_activation_key`, `user_status`, `Voornaam`, `Achternaam`, `Tussenvoegsel`, `Adres`, `Postcode`, `Woonplaats`, `Telefoonnummer`, `IBAN`, `Actief`) VALUES
(1, 1, 'Admin', '$2y$13$rlmTvkFxqgRU0p1Jpk0J.eqqBfuC/IGwV5em3h/TTZvr45ZW7HLrK', 4, '0000-00-00 00:00:00', '', '', 0, 'Ad', 'Admin', '', 'hsfsdffssfd', '4707DG', 'hsa', 'h', '', 0),
(7, 3, '', '', 0, '0000-00-00 00:00:00', '', '', 0, 'Thijs', 'Baas', '', 'Kastelenbuurt 123', '1234 BB', 'Amsterdorp', '0031612345678', '', 0),
(8, 2, '', '', 0, '0000-00-00 00:00:00', '', '', 0, 'Raymon', 'Eindbaas', 'de', 'Eindbazenstraat 21', '4321 AA', 'Amsterdam', '00316987654321', '', 0),
(9, 4, '', '', 0, '0000-00-00 00:00:00', '', '', 0, 'Lennart', 'Swag', 'van den', 'Appelsap 12', '1234 GG', 'Weerstation', '', '', 0),
(30, NULL, '', '', 0, '0000-00-00 00:00:00', '', '', 0, 'Richard', 'Wiet', '', 'Yolostraat 21', '1321 CC', 'Vlissingen', '', 'CY17002001280000001200527600', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_member_ship`
--

CREATE TABLE IF NOT EXISTS `oh_member_ship` (
  `Member_ID` int(11) DEFAULT NULL,
  `Ship_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_member_ship`
--

INSERT INTO `oh_member_ship` (`Member_ID`, `Ship_ID`) VALUES
(NULL, 1),
(NULL, 2),
(NULL, 3),
(NULL, 4),
(NULL, 5),
(NULL, 6),
(NULL, 7),
(NULL, 8),
(NULL, 9),
(NULL, 10),
(NULL, 11),
(NULL, 12),
(1, 13),
(1, 14),
(NULL, 15),
(NULL, 16),
(NULL, 17),
(NULL, 18),
(NULL, 19);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_price_category`
--

CREATE TABLE IF NOT EXISTS `oh_price_category` (
  `ID` int(11) NOT NULL,
  `Naam` varchar(50) NOT NULL DEFAULT '0',
  `PrijsPerEenheid` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_price_category`
--

INSERT INTO `oh_price_category` (`ID`, `Naam`, `PrijsPerEenheid`) VALUES
(1, 'Contributie', 0),
(2, 'Ligplaats', 15),
(3, 'Nieuw Schip Aanmelden', 1),
(4, 'lol', 2),
(5, 'laagtarief', 12);

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `oh_search_ship`
--
CREATE TABLE IF NOT EXISTS `oh_search_ship` (
`ID` int(11)
,`Naam` varchar(128)
,`Lengte` float
,`Voornaam` varchar(64)
,`Achternaam` varchar(64)
,`Tussenvoegsel` varchar(16)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_ships`
--

CREATE TABLE IF NOT EXISTS `oh_ships` (
  `ID` int(11) NOT NULL,
  `Naam` varchar(128) NOT NULL DEFAULT '0',
  `Lengte` float NOT NULL DEFAULT '0',
  `ImgURL` varchar(128) DEFAULT '0',
  `TrackingID` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_ships`
--

INSERT INTO `oh_ships` (`ID`, `Naam`, `Lengte`, `ImgURL`, `TrackingID`) VALUES
(1, 'fsdfsd', 234, NULL, 22),
(2, 'sdfdds', 334, NULL, 333),
(3, 'vvxc', 343, NULL, 343),
(4, 'fsdvsd', 213, NULL, 1232),
(5, 'fsdvsd', 213, NULL, 1232),
(6, 'sfdfsdf', 323, NULL, 3343),
(7, 'sfdfsdf', 323, NULL, 3343),
(8, 'fgdvgf', 312312, NULL, 232432),
(9, 'fgdvgf', 312312, NULL, 232432),
(10, 'dfsdf', 123, NULL, 213),
(11, 'dfsdf', 123, '/nieuwe-ouwe-have/images/ships/66694f0b237735974c6c517c1c4af57e9da037ec.PNG', 213),
(12, 'dfsdf', 123, '/nieuwe-ouwe-have/images/ships/f3f6f417beddb6ddf91716fa540ac70a925fd74b.PNG', 213),
(13, 'dfsdf', 123, '/nieuwe-ouwe-have/images/ships/72b7d0a7a3cdcb4d5226e2187353442a40113e34.PNG', 213),
(14, 'fgdgf', 23, NULL, 322),
(15, 'vxcv', 4543, NULL, 4534),
(16, 'vxcv', 4543, NULL, 4534),
(17, 'vxcv', 4543, NULL, 4534),
(18, 'vxcv', 4543, NULL, 4534),
(19, 'sdfsd', 2312, NULL, 232);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `oh_transactions`
--

CREATE TABLE IF NOT EXISTS `oh_transactions` (
  `ID` int(11) unsigned NOT NULL,
  `Categorie_ID` int(11) unsigned DEFAULT NULL,
  `Factuur_ID` int(11) DEFAULT NULL,
  `Rekeningnummer` varchar(35) DEFAULT NULL,
  `Muntsoort` varchar(3) DEFAULT NULL,
  `Rentedatum` int(10) DEFAULT NULL,
  `Code` char(1) DEFAULT NULL,
  `Bedrag` float DEFAULT NULL,
  `Tegenrekening` varchar(35) DEFAULT NULL,
  `Naam_Ontvanger` varchar(70) DEFAULT NULL,
  `Boekdatum` int(10) DEFAULT NULL,
  `Boekcode` varchar(2) DEFAULT NULL,
  `Filler` varchar(6) DEFAULT NULL,
  `Omschrijving` varchar(210) DEFAULT NULL,
  `End_To_End_ID` varchar(35) DEFAULT NULL,
  `Tegenrekeninghouder_ID` varchar(35) DEFAULT NULL,
  `Mandaat_ID` varchar(35) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `oh_transactions`
--

INSERT INTO `oh_transactions` (`ID`, `Categorie_ID`, `Factuur_ID`, `Rekeningnummer`, `Muntsoort`, `Rentedatum`, `Code`, `Bedrag`, `Tegenrekening`, `Naam_Ontvanger`, `Boekdatum`, `Boekcode`, `Filler`, `Omschrijving`, `End_To_End_ID`, `Tegenrekeninghouder_ID`, `Mandaat_ID`) VALUES
(1, 3, NULL, 'NL11RABO0123456789', 'EUR', 1428650507, 'D', 35.07, 'NL85KNAB0464567824', 'MultiSafepay', 1428650507, 'id', '', 'Yolo', '', '', ''),
(2, 2, NULL, 'NL11RABO0123456789', 'EUR', 1428650507, 'C', 353510, 'NL45INGB0456789314', 'Hr T Swag', 1428650507, 'cb', '', 'Katjes', '', '', ''),
(3, NULL, NULL, 'NL11RABO0123456789', 'EUR', 1428667537, 'D', 35.07, 'NL45INGB0456789314', 'MultiSafepay', 1428667537, 'id', '', 'Yolo', '', '', ''),
(4, NULL, NULL, 'NL11RABO0123456789', 'EUR', 1428667537, 'C', 10.24, 'NL85KNAB0464567824', 'Hr T Swag', 1428667537, 'cb', '', 'Katjes', '', '', '');

-- --------------------------------------------------------

--
-- Structuur voor de view `oh_search_ship`
--
DROP TABLE IF EXISTS `oh_search_ship`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `oh_search_ship` AS select `oh_ships`.`ID` AS `ID`,`oh_ships`.`Naam` AS `Naam`,`oh_ships`.`Lengte` AS `Lengte`,`oh_members`.`Voornaam` AS `Voornaam`,`oh_members`.`Achternaam` AS `Achternaam`,`oh_members`.`Tussenvoegsel` AS `Tussenvoegsel` from ((`oh_member_ship` left join `oh_members` on((`oh_member_ship`.`Member_ID` = `oh_members`.`ID`))) left join `oh_ships` on((`oh_member_ship`.`Ship_ID` = `oh_ships`.`ID`)));

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `oh_balances`
--
ALTER TABLE `oh_balances`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `oh_cashbook`
--
ALTER TABLE `oh_cashbook`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `oh_categories`
--
ALTER TABLE `oh_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `oh_contributie`
--
ALTER TABLE `oh_contributie`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `oh_invoices`
--
ALTER TABLE `oh_invoices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_members_ID` (`Lid_ID`);

--
-- Indexen voor tabel `oh_invoices_line`
--
ALTER TABLE `oh_invoices_line`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_invoices_ID` (`Factuur_ID`),
  ADD KEY `FK_category_ID` (`Categorie_ID`);

--
-- Indexen voor tabel `oh_members`
--
ALTER TABLE `oh_members`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_oh_members_oh_users` (`User_ID`);

--
-- Indexen voor tabel `oh_member_ship`
--
ALTER TABLE `oh_member_ship`
  ADD KEY `FK__oh_members` (`Member_ID`),
  ADD KEY `FK__oh_ships` (`Ship_ID`);

--
-- Indexen voor tabel `oh_price_category`
--
ALTER TABLE `oh_price_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `oh_ships`
--
ALTER TABLE `oh_ships`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `oh_transactions`
--
ALTER TABLE `oh_transactions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_oh_transactions_oh_categories` (`Categorie_ID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `oh_balances`
--
ALTER TABLE `oh_balances`
  MODIFY `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `oh_cashbook`
--
ALTER TABLE `oh_cashbook`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT voor een tabel `oh_categories`
--
ALTER TABLE `oh_categories`
  MODIFY `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `oh_contributie`
--
ALTER TABLE `oh_contributie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `oh_invoices`
--
ALTER TABLE `oh_invoices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT voor een tabel `oh_invoices_line`
--
ALTER TABLE `oh_invoices_line`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT voor een tabel `oh_members`
--
ALTER TABLE `oh_members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT voor een tabel `oh_price_category`
--
ALTER TABLE `oh_price_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `oh_ships`
--
ALTER TABLE `oh_ships`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT voor een tabel `oh_transactions`
--
ALTER TABLE `oh_transactions`
  MODIFY `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `oh_invoices`
--
ALTER TABLE `oh_invoices`
  ADD CONSTRAINT `FK_members_ID` FOREIGN KEY (`Lid_ID`) REFERENCES `oh_members` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `oh_invoices_line`
--
ALTER TABLE `oh_invoices_line`
  ADD CONSTRAINT `FK_category_ID` FOREIGN KEY (`Categorie_ID`) REFERENCES `oh_price_category` (`ID`),
  ADD CONSTRAINT `FK_invoices_ID` FOREIGN KEY (`Factuur_ID`) REFERENCES `oh_invoices` (`ID`);

--
-- Beperkingen voor tabel `oh_member_ship`
--
ALTER TABLE `oh_member_ship`
  ADD CONSTRAINT `FK__oh_members` FOREIGN KEY (`Member_ID`) REFERENCES `oh_members` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__oh_ships` FOREIGN KEY (`Ship_ID`) REFERENCES `oh_ships` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `oh_transactions`
--
ALTER TABLE `oh_transactions`
  ADD CONSTRAINT `FK_oh_transactions_oh_categories` FOREIGN KEY (`Categorie_ID`) REFERENCES `oh_categories` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
